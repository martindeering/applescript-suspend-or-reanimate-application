# README #

### Description ###
Some applications do not have a pause button. They take CPU processing power and will not let it go until their job is finished. Other applications (particularly browsers) tend to have pages that will continue to process or refresh in the background, even when the browser is hidden. Apart from quitting these applications, there is another solution: Using the kill -STOP command via the Terminal.
If you open Activity Monitor, you will notice that the suspended application is now taking 0% CPU power, freeing up the CPU.

### What is this repository for? ###

* Applescript to Suspend and Reanimate Applications
* Version 1.0

### How do I get set up? ###

* Copy the code, paste into Script Editor, save as .scpt or as an application. Double-click to run.
* Run the script. Enter the full name of the application (as it appears in the Applications folder). Pressing OK will hide and suspend the application. At this point, clicking the application icon in the dock will not do anything. The application will be unresponsive until it is re-animated. To reanimate, run the script and enter the full name of the application that is suspended.
* Modifications. The app has one entry method: A dialog box asking for the name of the application. The script itself has the option of three different input methods. Text entry, choose from buttons, choose from list. Basically, if you are regularly suspending one or more applications the second or third inputs will be quicker, with no need for typing. You can then save that configuration as an application, or put it in the scripts folder in the menu bar (which is where I put it).
* More info in the comments in the script.

### Other Notes / Warnings ###
* As with any Applescript app, sometimes the dialog box gets hidden behind other windows.
* The script will warn you if you try to suspend the Finder or the Dock. Use this at your own risk. I've included error handlers and a 'copy' button for the Terminal command should the script fail.
* If you suspend an application, and then later try to restart/shut-down, the system will notice the suspended application and see that it is unresponsive. This will throw up the 'Force Quit' dialogs. To avoid this, reanimate the application before restarting/shutting-down.

### Who do I talk to? ###

* Repo owner